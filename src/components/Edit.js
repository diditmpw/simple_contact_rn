import React, { Component } from 'react';
import { Text, View, TouchableHighlight, TextInput, Alert } from 'react-native';

class Edit extends Component {
    constructor(props) {
        super(props)
        this.state = {
            error: null,
            isLoaded: false,
            firstName: '',
            lastName: '',
            age: '',
            photo: '',
        };
    }

    componentDidMount() {
        fetch("https://simple-contact-crud.herokuapp.com/contact/" + this.props.id)
            .then(res => res.json())
            .then(
                (result) => {
                    console.log(result)
                    this.setState({
                        isLoaded: true,
                        firstName: result.data.firstName,
                        lastName: result.data.lastName,
                        age: result.data.age.toString(),
                        photo: result.data.photo,
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    submitData() {
        if (!(this.state.firstName && this.state.lastName && this.state.age > 0 && this.state.photo)) {
            Alert.alert("Wrong input data")
            return false
        }
        this.props.onClickSubmit(
            {
                id: this.props.id,
                body: {
                    firstName: this.state.firstName,
                    lastName: this.state.lastName,
                    age: this.state.age,
                    photo: this.state.photo,
                }
            }
        )
    }


    render() {
        const { error, isLoaded, items } = this.state;
        if (error) {
            return <View style={{ marginHorizontal: 20, marginVertical: 50 }}><Text>Error: {error.message}</Text></View>;
        } else if (!isLoaded) {
            return <View style={{ marginHorizontal: 20, marginVertical: 50 }}><Text>Loading...</Text></View>;
        } else {

            return (
                <View style={{ marginHorizontal: 20, marginVertical: 50 }}>
                    <View><Text style={{ fontSize: 30, fontWeight: "bold" }}>Edit</Text></View>
                    <View style={{ flexDirection: "row", margin: 5 }}>
                        <Text>First Name:</Text><TextInput value={this.state.firstName} onChangeText={(firstName) => { this.setState({ firstName }) }} style={{ flex: 1, backgroundColor: "gray" }} />
                    </View>
                    <View style={{ flexDirection: "row", margin: 5 }}>
                        <Text>Last Name:</Text><TextInput value={this.state.lastName} onChangeText={(lastName) => { this.setState({ lastName }) }} style={{ flex: 1, backgroundColor: "gray" }} />
                    </View>
                    <View style={{ flexDirection: "row", margin: 5 }}>
                        <Text>Age:</Text><TextInput value={this.state.age} onChangeText={(age) => { this.setState({ age }) }} keyboardType="numeric" style={{ flex: 1, backgroundColor: "gray" }} />
                    </View>
                    <View style={{ flexDirection: "row", margin: 5 }}>
                        <Text>Picture:</Text><TextInput value={this.state.photo} onChangeText={(photo) => { this.setState({ photo }) }} style={{ flex: 1, backgroundColor: "gray" }} />
                    </View>
                    <View style={{ flexDirection: "row", margin: 5 }}>
                        <TouchableHighlight onPress={() => { this.props.onClickCancel() }} style={{ flex: 1, backgroundColor: "gray", margin: 5 }} ><Text style={{ textAlign: "center" }} >Cancel</Text></TouchableHighlight><TouchableHighlight onPress={() => { this.submitData() }} style={{ flex: 1, backgroundColor: "gray", margin: 5 }} ><Text style={{ textAlign: "center" }} >Submit</Text></TouchableHighlight>
                    </View>
                </View>
            )
        }
    }
}

export default Edit;