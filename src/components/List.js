import React, { Component } from 'react';
import { Text, View, TouchableHighlight, ScrollView } from 'react-native';


class List extends Component {

    constructor(props) {
        super(props)
        this.state = {
            error: null,
            isLoaded: false,
            items: []
        };
    }

    componentDidMount() {
        fetch("https://simple-contact-crud.herokuapp.com/contact")
            .then(res => res.json())
            .then(
                (result) => {
                    console.log(result)
                    this.setState({
                        isLoaded: true,
                        items: result.data
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    render() {
        const { error, isLoaded, items } = this.state;
        if (error) {
            return <View style={{ marginHorizontal: 20, marginVertical: 50 }}><Text>Error: {error.message}</Text></View>;
        } else if (!isLoaded) {
            return <View style={{ marginHorizontal: 20, marginVertical: 50 }}><Text>Loading...</Text></View>;
        } else {

            return (
                <ScrollView style={{ marginHorizontal: 20, marginVertical: 50 }}>
                    <TouchableHighlight style={{ backgroundColor: 'gray', padding: 5 }} onPress={() => { this.props.onClickAdd() }}><Text>Add</Text></TouchableHighlight>
                    <View>
                        {
                            items.map(item => (
                                <View style={{ padding: 5 }}  key={item.id}>
                                    <View style={{flexDirection:"row"}}>
                                        <Text style={{ padding: 5 }} >{item.firstName} {item.lastName}</Text>
                                        <TouchableHighlight style={{ backgroundColor: 'gray', padding: 5 }}  onPress={() => { this.props.onClickEdit(item.id) }}><Text>Edit</Text></TouchableHighlight>
                                        <TouchableHighlight style={{ backgroundColor: 'gray', padding: 5 }}  onPress={() => { this.props.onClickDelete(item.id) }}><Text>Delete</Text></TouchableHighlight>
                                    </View>
                                    <View>
                                        <Text>ID: {item.id}</Text>
                                        <Text>First Name: {item.firstName}</Text>
                                        <Text>Last Name: {item.lastName}</Text>
                                        <Text>Age: {item.age}</Text>
                                        <Text>Picture: {item.photo}</Text>
                                    </View>
                                </View>
                            ))
                        }
                    </View>
                </ScrollView>
            );

        }

    }

}

export default List;