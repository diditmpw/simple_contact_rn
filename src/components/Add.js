import React, { Component } from 'react';
import { Text, View, TouchableHighlight, TextInput, Alert } from 'react-native';


class Add extends Component {

    constructor(props) {
        super(props)
        this.state = {
            firstName: '',
            lastName: '',
            age: '',
            photo: '',
        };
    }

    submitData() {
        if (!(this.state.firstName && this.state.lastName && this.state.age>0 && this.state.photo)) {
            Alert.alert("Wrong input data")
            return false
        }
        this.props.onClickSubmit(
            {
                id: 0,
                body: {
                    firstName: this.state.firstName,
                    lastName: this.state.lastName,
                    age: this.state.age,
                    photo: this.state.photo,
                }
            }
        )
    }

    render() {
        return (
            <View style={{ marginHorizontal: 20, marginVertical: 50 }}>
                <View><Text style={{fontSize:30, fontWeight:"bold"}}>Add New</Text></View>
                <View style={{ flexDirection: "row", margin: 5 }}>
                    <Text>First Name:</Text><TextInput onChangeText={(firstName) => { this.setState({ firstName }) }} style={{ flex: 1, backgroundColor: "gray" }} />
                </View>
                <View style={{ flexDirection: "row", margin: 5 }}>
                    <Text>Last Name:</Text><TextInput onChangeText={(lastName) => { this.setState({ lastName }) }} style={{ flex: 1, backgroundColor: "gray" }} />
                </View>
                <View style={{ flexDirection: "row", margin: 5 }}>
                    <Text>Age:</Text><TextInput onChangeText={(age) => { this.setState({ age }) }} keyboardType="numeric" style={{ flex: 1, backgroundColor: "gray" }} />
                </View>
                <View style={{ flexDirection: "row", margin: 5 }}>
                    <Text>Picture:</Text><TextInput onChangeText={(photo) => { this.setState({ photo }) }} style={{ flex: 1, backgroundColor: "gray" }} />
                </View>
                <View style={{ flexDirection: "row", margin: 5 }}>
                    <TouchableHighlight onPress={() => { this.props.onClickCancel() }} style={{ flex: 1, backgroundColor: "gray", margin: 5 }} ><Text style={{ textAlign: "center" }} >Cancel</Text></TouchableHighlight><TouchableHighlight onPress={() => { this.submitData()}} style={{ flex: 1, backgroundColor: "gray", margin: 5 }} ><Text style={{ textAlign: "center" }} >Submit</Text></TouchableHighlight>
                </View>
            </View>
        )
    }
}

export default Add;