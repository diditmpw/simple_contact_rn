import React, { Component } from 'react';
import { Text, View, TouchableHighlight, TextInput, Alert } from 'react-native';

class Delete extends Component {
    constructor(props) {
        super(props)
        this.state = {
            error: null,
            isLoaded: false,
            item: {}
        };
    }

    componentDidMount() {
        fetch("https://simple-contact-crud.herokuapp.com/contact/" + this.props.id)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        item: result.data
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    submitData() {
        this.props.onClickSubmit(this.props.id)
    }

    render() {
        const { error, isLoaded, item } = this.state;
        if (error) {
            return <View style={{ marginHorizontal: 20, marginVertical: 50 }}><Text>Error: {error.message}</Text></View>;
        } else if (!isLoaded) {
            return <View style={{ marginHorizontal: 20, marginVertical: 50 }}><Text>Loading...</Text></View>;
        } else {

            return (
                <View style={{ marginHorizontal: 20, marginVertical: 50 }}>
                    <View><Text style={{ fontSize: 30, fontWeight: "bold" }}>Delete</Text></View>
                    <View style={{ flexDirection: "row", margin: 5 }}>
                        <Text>First Name: {item.firstName}</Text>
                    </View>
                    <View style={{ flexDirection: "row", margin: 5 }}>
                        <Text>Last Name: {item.lastName}</Text>
                    </View>
                    <View style={{ flexDirection: "row", margin: 5 }}>
                        <Text>Age: {item.age}</Text>
                    </View>
                    <View style={{ flexDirection: "row", margin: 5 }}>
                        <Text>Picture: {item.photo}</Text>
                    </View>
                    <View style={{ flexDirection: "row", margin: 5 }}>
                        <TouchableHighlight onPress={() => { this.props.onClickCancel() }} style={{ flex: 1, backgroundColor: "gray", margin: 5 }} ><Text style={{ textAlign: "center" }} >Cancel</Text></TouchableHighlight><TouchableHighlight onPress={() => { this.submitData() }} style={{ flex: 1, backgroundColor: "gray", margin: 5 }} ><Text style={{ textAlign: "center" }} >Submit</Text></TouchableHighlight>
                    </View>
                </View>
            )
        }
    }
}

export default Delete;