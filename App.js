/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';

import List from './src/components/List';
import Add from './src/components/Add';
import Edit from './src/components/Edit';
import Delete from './src/components/Delete';

class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      screen: 'list',
      id: 0
    }
  }

  onCancel() {
    this.setState(
      {
        screen: 'list'
      }
    )
  }

  onAddSelect() {
    this.setState(
      {
        screen: 'add'
      }
    )
  }

  onEditSelect(id) {
    this.setState(
      {
        screen: 'edit',
        id: id
      }
    )
  }

  onDeleteSelect(id) {
    this.setState(
      {
        screen: 'delete',
        id: id
      }
    )
  }

  onAddSubmit(data) {
    fetch("https://simple-contact-crud.herokuapp.com/contact", {
      method: 'post',
      body: JSON.stringify(data.body)
    })
      .then(res => res.json())
      .then(
        (result) => {
          console.log(result)
        },
        (error) => {
          console.log(error)
        }
      )
      .then(
        this.setState(
          {
            screen: 'list'
          }
        )
      )
  }

  onEditSubmit(data) {
    fetch("https://simple-contact-crud.herokuapp.com/contact/" + data.id, {
      method: 'put',
      body: JSON.stringify(data.body)
    })
      .then(res => res.json())
      .then(
        (result) => {
          console.log(result)
        },
        (error) => {
          console.log(error)
        }
      )
      .then(
        this.setState(
          {
            screen: 'list'
          }
        )
      )
  }

  onDeleteSubmit(id) {
    fetch("https://simple-contact-crud.herokuapp.com/contact/" + id, {
      method: 'delete'
    })
      .then(res => res.json())
      .then(
        (result) => {
          console.log(result)
        },
        (error) => {
          console.log(error)
        }
      )
      .then(
        this.setState(
          {
            screen: 'list'
          }
        )
      )
  }

  render_screen() {
    switch (this.state.screen) {
      case 'list': {
        return <List
          onClickAdd={() => { this.onAddSelect() }}
          onClickEdit={(id) => { this.onEditSelect(id) }}
          onClickDelete={(id) => { this.onDeleteSelect(id) }}
        />
      }
        break;
      case 'add': {
        return <Add
          onClickCancel={() => { this.onCancel() }}
          onClickSubmit={(data) => { this.onAddSubmit(data) }}
        />
      }
        break;
      case 'edit': {
        return <Edit
          id={this.state.id}
          onClickCancel={() => { this.onCancel() }}
          onClickSubmit={(data) => { this.onEditSubmit(data) }}
        />
      }
        break;
      case 'delete': {
        return <Delete
          id={this.state.id}
          onClickCancel={() => { this.onCancel() }}
          onClickSubmit={(id) => { this.onDeleteSubmit(id) }}
        />
      }
        break;
    }
  }
  render() {
    return (
      <View>
        {this.render_screen()}
      </View>
    )
  }
}

export default App;